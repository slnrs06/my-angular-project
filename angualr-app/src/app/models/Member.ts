export interface Member {             // export should be included
  firstName: string;
  lastName: string;
  username: string;
  memberNo: number;
}
