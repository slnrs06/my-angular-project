export interface User {
  firstName: string,
  lastName: string,
  age?: number,
  email: string,

  // address?: {        // is object holding 3 props
  //   street: string,
  //   city:  string,
  //   state: string
  // },
  image?: string,
  isActive?: boolean,
  balance?: number,
  memberSince?: any,
  hide?: boolean;
  showUserForm?: boolean;
}
