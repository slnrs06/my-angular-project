import {Component} from "@angular/core";
import {User} from "../../models/User";
import {hasI18nAttrs} from "@angular/compiler/src/render3/view/i18n/util";

@Component({
  selector: 'app-user',
  template: '<h1>Hello</h1>',
    // './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent {
  // PROPERTIES -like an attribute of the Component
    firstName = 'Clark';
    lastName = 'Kent';
    age = 65;

  // MORE PRACTICAL WAY OF CREATING PROPERTIES
  // SYNTAX:  propertyName: datatype
  //     firstName: string;       // datatype
  //     lastName: string;
  //     age: number;
  //     whatever: any;
  //     hasKids: boolean;

//    DATA TYPES -------
//       numberArray: number[];  // this MUST BE AN ARRAY OF NUMBERS
//       stringArray: string[];  // THIS MUST BE AN ARRAY OF
//       mixArray: any[];      // THIS CAN BE AN ARRAY OF ANYTHING
//

  //  MIGHT SEE THIS MORE OFTEN
  //     user: User;

//  METHODS ---  runs automatically     a 'Function' inside of a class/ this. keyword to access props

  //   CONSTRUCTOR - RUNS EVERY TIME WHEN OUR COMPONENT IS INITIALIZED / CALLED/dependency
  // constructor() {
    // console.log('Hello from UserComponent');
    //
    // this.greeting();
    // console.log(this.age);   // 65 /// return undefined
    // this.hasBirthday();
    // console.log(this.age)    // 66      // return 31

    // this.firstName = 'Clark';
    // this.lastName = 'Kent';
    // this.age = 65;
    // this.hasKids = false;
    // this.greeting();
    //
    // this.numberArray = [12,33,24];
    // this.stringArray = ['12', '34', '24'];
    // this.mixArray = [12, 'stephen', true];

    // CONNECTING WITH INTERFACE
  //   this.user = {
  //     firstName: 'Jane',
  //     lastName: 'Doe',
  //     age: 30,
  //     address: {
  //       street: '150 Alamo Plaza',
  //       city: 'San Antonio',
  //       state: 'TX'
  //     }
  //   }
  // }

  //
  // greeting(){
  //   // console.log('Hello there, ' + this.firstName + ' ' + this.lastName);
  //   //
  //   return `Hello there, ${this.user.firstName} ${this.user.lastName}`;
  // }
  //
  // hasBirthday(){
    // this.age = 30;
    // return this.age +=1;
    // this.age += 1;
//   }
//
} // END OF CLASS (DON'T DELETE)

//   ALTER WAY TO CREATE AN INTERFACE ------
// interface member {
//   firstName: string,
//   lastName: string,
//   age: number,
//   address: {        // is object holding 3 props
//     street: string,
//     city:  string,
//     state: string
//   }
// }
