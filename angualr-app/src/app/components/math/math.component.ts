import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-math',
  templateUrl: './math.component.html',
  styleUrls: ['./math.component.css']
})
export class MathComponent implements OnInit {
  // PROPERTIES
    num1: number;
    num2: number;

  constructor() {
    // this.num1 = 5;
    // this.num2 = 10;
    // this.sum();
    // this.addNumbers(x5, y10);    --  stephen answer
    // this.subtract(a,b)
    //this.multiply(a,b);

    // console.log(this.divide(a, b));   // because returned used

    // this.addNumbers(10,5)
  }

  // sum(){
    // console.log(this.num1 + this.num2);
    // console.log(this.num1 - this.num2);

  // }


  ngOnInit(): void {
  }
addNumbers(x, y) {
  this.num1 = x;
  this.num2 = y;
  return this.num1 + this.num2;
}
  // console.log(this.num1 + this.num2);

  subtract(x, y) {
   this.num1 = x;
   this.num2 = y;
    return this.num1 - this.num2;
  }

  multiply(x, y,){
     this.num1 = x;
     this.num2 = y;
   return this.num1 * this.num2;
  }

  divide(x, y,){
     this.num1 = x;
     this.num2 = y;
   return this.num1 / this.num2;
  }

  //  Adrians answer:
  // table(){
  //   for (var i=1;i<100;i+=1) {
  //     if (i % 3 == 0 && i % 15 != 0) {
  //       console.log('Fizz');
  //     } else if (i % 5 == 0 && i % 15 !=0) {
  //       console.log('Buzz');
  //     } else if ( i % 15 == 0) {
  //       console.log('FizzBuzz');
  //     } else {
  //       console.log(i);
  //     }
  //   }



}
// END OF CLASS   DONT DELETE




//
// Exercise #3
// Create a new component named 'math' in your components directory
// In your math.component.ts, create two properties, 'num1' and 'num2'.
//   Set both properties as a number.
//   Next, create a method that takes in the two properties as inputs and returns
//   the sum of the two properties.
//   Console log the result
// Create a method that takes in the two properties and returns the difference of
// the two numbers.
//   Console log the result
// Create a method that takes in the two properties and returns the product of
// the two numbers.
//   Console log the result
// Create a method that takes in the two properties and returns the quotient
// of the two numbers. Console log the result
// BONUS: Write a program that prints the numbers from 1 to 100. But for multiples
// of three, print 'Fizz' instead of the number. For the multiples of five point, print 'Buzz'.
// And for the numbers which are mulitples of both three and five, print 'FizzBuzz'
