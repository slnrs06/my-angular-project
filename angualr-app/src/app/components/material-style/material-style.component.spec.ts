import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialStyleComponent } from './material-style.component';

describe('MaterialStyleComponent', () => {
  let component: MaterialStyleComponent;
  let fixture: ComponentFixture<MaterialStyleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialStyleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialStyleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
