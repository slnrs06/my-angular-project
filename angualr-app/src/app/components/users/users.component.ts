import { Component, OnInit, ViewChild } from '@angular/core';
import {User} from "../../models/User";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  // PROPERTIES
  user: User ={
    firstName: '',
    lastName: '',
    age: null,
    email: '',
    balance: 0,
    hide: false,
    image: "",
    isActive: false,
    memberSince: undefined,
    showUserForm: false,

  };

  users: User[];  //'Users' is like setting a variable
// myNumberArray: number[],
  displayInfo: boolean = true;
  showUserForm: boolean = false;
  enableAddUser: boolean;
  currentClass: {};  //basically an empty object
  currentStyle: {};
  @ViewChild("userForm")form: any;

  // CREATE A PROPERTY FOR THE OBSERVABLE
      data: any;

  //    users: User[];
  //    displayInfo: boolean = true;
  //    enableAddUser: boolean;
  //
  //    // mynumberArray: number[];
  //    // myString


  //  INJECT THE SERVICE AS A DEPENDENCY IN OUR CONSTRUCTOR
  constructor(private dataService: DataService) { }

  ngOnInit(): void {

    // this.users = this.dataService.getUsers();
    // SUBSCRIBE to the observable to use getUsers() method....
    this.dataService.getUsers().subscribe(users => {
      this.users = users;
    })

    //subscribe to the observable ----
    //   this.dataService.getData().subscribe(data => {
    //     console.log(data)
    //   });

    this.enableAddUser = true;

    this.setCurrentClasses();
    this.setCurrentStyle();

    // END OF OUR ARRAY

      // CALLING OUR ADDUSER()
    // this.addUser({
    //   firstName: 'Jason',
    //     lastName: 'Todd',
    //     age: 13
    // });

  } ///  END OF NGONINIT ()   (DONT DELETE)

// CREATE a method that adds a new user to the array

  //Create a method that will change the state of our button
  setCurrentClasses() {
    this.currentClass = {
      'btn-dark': this.enableAddUser,
    }
  };
  //Create a method that will add padding to the users name
  // when the users info is not displaying (this.displayInfo = false)
  setCurrentStyle(){
    this.currentStyle = {
      'padding-top': this.displayInfo ? '0' : '80px'
    }
  }
//adding in fireEvent method under click event
  //TYPE OF EVENTS-
  /*(mousedown), (mouseup), (mouseout), (click), (dblclick), (drag)  */
  fireEvent(e){
    console.log('The event has just happened');
    // console.log(e.type);
    // console.log("The dragover has been activated");
  }
  onSubmit({value,valid}:{value:User, valid:boolean}) {
    if(!valid){
       alert("Your Form is invalid!!!!")
    }else {
      value.isActive = true
      value.memberSince = new Date();
      value.hide = true;

    // FAITH code from forms
      // this.users.unshift(value);

      // stephens code from services
        this.dataService.addUser(value);

      //resets the form
      this.form.reset();
    }
    // console.log(321);
    // e.preventDefault();
  }

  // fireEvent(e){
  //   console.log(e.type);
  //   console.log(e.target.value)
  // }

  toggleHide(user: User){
    user.hide = !user.hide;
  }




} // END OF CLASS  DONT DELETE
