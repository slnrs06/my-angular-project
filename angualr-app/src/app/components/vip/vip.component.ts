import { Component, OnInit } from '@angular/core';
import{Member} from "../../models/Member";

@Component({
  selector: 'app-vip',
  templateUrl: './vip.component.html',
  styleUrls: ['./vip.component.css']
})
export class VipComponent implements OnInit {
  members: Member[];

  // displayInfo: boolean = false;
  loadingMembers: boolean = false;

  constructor() {
  }

  ngOnInit(): void {
    setTimeout(() => {this.loadingMembers = true}, 5000);

    this.members = [{
      firstName: 'Maryann',
      lastName: 'Frias',
      username: 'mann',
      memberNo: 111

    },
      {
        firstName: 'Henry',
        lastName: 'Ford',
        username: 'hff',
        memberNo: 222

      },
      {
        firstName: 'Jonathan',
        lastName: 'Robles',
        username: 'jbb',
        memberNo: 333

      },
      {
        firstName: 'Adrian',
        lastName: 'Arizpe',
        username: 'azp',
        memberNo: 444

      },
      {
        firstName: 'Eric',
        lastName: 'Chapa',
        username: 'erc',
        memberNo: 555

      },


    ];

  }



}
