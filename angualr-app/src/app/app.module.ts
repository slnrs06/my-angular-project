import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";
import { HttpClientModule, HttpHeaders } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';

import {MatButtonModule} from '@angular/material/button';

import {MatInputModule} from '@angular/material/input';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';


import { AppComponent } from './app.component';
import { UserComponent} from "./components/user/user.component";
import {HelloComponent} from "./components/hello/hello.component";
import {TravelComponent} from "./components/travel/travel.component";
import { CodeboundComponent } from './components/codebound/codebound.component';
import { MathComponent } from './components/math/math.component';
import { VipComponent } from './components/vip/vip.component';
import {UsersComponent} from "./components/users/users.component";

import { PostsComponent } from './components/posts/posts.component';


//SERVICES
import{ DataService} from "./services/data.service";
import { PostsService} from "./services/posts.service";
import { PostFormComponent } from './components/post-form/post-form.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialStyleComponent } from './components/material-style/material-style.component';




@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    HelloComponent,
    TravelComponent,
    CodeboundComponent,
    MathComponent,
    VipComponent,
    UsersComponent,
    PostsComponent,
    PostFormComponent,
    HomeComponent,
    NavbarComponent,
    MaterialStyleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule
  ],
  providers: [
    DataService,
    PostsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }



//  TEMPLATE FORM//  creating a component external template form  /// reactive
// import { Component } from '@angular/core';
// import{BrowserModule} from "@angular/platform-browser";
// import{NgModule}from '@angular/core';
// import {FormsModule} from "@angular/forms";
// import {AppComponent} from './app.component';
//
// @NgModule({
//   declarations: [AppComponent
//   ],
//   imports:[
//     BrowserModule,
//     FormsModule
//   ],
//   providers:[],
//   bootstrap: [AppComponent]
//
// })
// export class AppModule{}


/// REACTIVE FORMS EXAMPLE *******
//
// import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// import {NgModule} from '@angular/core';
// import {BrowserModule} from '@angular/platform-browser';
// import { AppComponent } from './app.component';
// import { UsersComponent } from './components/users/users.component';
// @NgModule({
//   declarations: [// components go
//     AppComponent, UsersComponent
//   ],
//   imports: [ // modules go here
//     // other imports ...
//     ReactiveFormsModule,
//     BrowserModule,
//     FormsModule
//   ],
//   providers: [], // services go here
//   bootstrap: [AppComponent]
// })
// export class AppModule { }
