import { Injectable } from '@angular/core';
import {User} from "../models/User";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataService {
// INITIALIZE A USER'S PROPERTY AND ASSIGN IT TO AN ARRAY OF USER
  users: User[];

  data: Observable<any>;


  constructor() {
    this.users = [
      {
        firstName:  'Bruce',
        lastName: 'Wayne',
        age: 30,
        email: "bruce@gmail.com",
        // address: {
        //   street: '100 Wayne Manor Dr',
        //   city: 'Gotham City',
        //   state: 'New York'
        //
        // },
        image: '../../assets/img/bruceWayne.jpg',

      },
      {
        firstName: 'Diana',
        lastName: 'Prince',
        age: 100,
        email: 'diana@gmail.com',
        // address: {
        //   street: '100 Wayne Manor Dr',
        //   city: 'Gotham City',
        //   state: 'New York'
        // },
        isActive: true
      }

  ];  // end of this.users array

  }// end of constructor


  // getData(){
  //   this.data = new Observable(observer => {
  //     setTimeout(() => {
  //       observer.next(1)
  //     }, 1000);
  //
  //     setTimeout(() => {
  //       observer.next(2)
  //     }, 2000);
  //
  //     setTimeout(() => {
  //       observer.next(3)
  //     }, 3000);
  //
  //     setTimeout(() => {
  //       observer.next("stephen")
  //     }, 4000);
  //   })
  //   return this.data;
  // }

  // getUsers(), return a type of User[]

  //refactor our method to return an array as an Observable
  getUsers(): Observable<User[]> {
    // alert('Fetching users from service');
    console.log('Fetching users from service')
    return of(this.users);
  }
  // CREATE a method that adds a new user to the array//.push - is at the end of array
  addUser(user: User) {
    console.log('Added user from service');
    this.users.unshift(user);
  }

}
